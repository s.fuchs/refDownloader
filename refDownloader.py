#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# A manuscript describing this program is in preparation. However, as long as it has been not published please contact Stephan Fuchs (fuchss@rki.de) for redistributing and/or modifying this program.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# Using this program you accept NCBI's Website and Data Usage Policies and Disclaimers (https://www.ncbi.nlm.nih.gov/home/about/policies.shtml).
# copyright (c) 2016 Stephan Fuchs (fuchss@rki.de)



#config
version = '3.2.2'
logfilename = "refDownloader.log"
logsep = '=====================================\n'
exclude_files = ['README.txt', 'md5checksums.txt', 'annotation_hashes.txt']

##history

#dependencies
try:
    import argparse
except ImportError:
    print('The "argparse" module is not available. Use Python 3.2 or better.')
    sys.exit(1)

import os
import sys
import time
import difflib
import multiprocessing
import subprocess
import logging
import itertools
import urllib.request
import gzip
import traceback
import re
import socket
from datetime import datetime
from ftplib import FTP, all_errors
import hashlib

#config
ftp_url={}
ftp_url['genbank'] = "ftp.ncbi.nlm.nih.gov/genomes/genbank/bacteria/"
ftp_url['refseq'] = "ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/"
as_file = 'assembly_summary.txt'
logging = {}
disclaimer = "Using refDownloader you accept NCBI's Website and Data Usage Policies and Disclaimers (https://www.ncbi.nlm.nih.gov/home/about/policies.shtml)."

#processing command line arguments
parser = argparse.ArgumentParser(prog="RefDownloader", description="downloads all LATEST bacterial genome and plasmid sequences from Refseq or Genbank. " + disclaimer, )
arg0group = parser.add_mutually_exclusive_group(required=True)
arg0group.add_argument('--species', metavar="STR", help="input species name(s) enclosed by apostrophs (e.g. 'Staphylococcus aureus').", type=str, nargs="+")
arg0group.add_argument('--regex', metavar="STR", help="use regular expression for species selection", type=str)
arg0group.add_argument('--ftp', metavar="STR", help="input ftp path(s) (under this path an " + as_file + " file has to exist). Multiple paths have to belong to the same domain.", type=str, nargs="+")
parser.add_argument('--email', metavar="STR", help="necessary to regularly login to NCBI FTP server for batch downloading", type=str, required=True)
parser.add_argument('--db', metavar="STR", help="select refseq or genbank as database to download from (by default: refseq). This option is ignored if --ftp is used.", choices=['refseq', 'genbank'], default='refseq')
parser.add_argument('--sort', help="*_genomic.fna.gz files may contain chromosomal and plasmidic entrys. If this option is set, refDownloader unpacks that files and groups entries based on source. Originial file is not deleted.", action='store_true')
parser.add_argument('--ignore', help="ignore all non-existen ftp paths or species names", action='store_true')
parser.add_argument('-t', metavar="INT", help="number of parallel downloads (by default: 1; max: 3)", type=int, default='1')
parser.add_argument('--version', action='version', version='%(prog)s ' + version)
args = parser.parse_args()


#FUNCTIONS
def init(l, q, d):
	global lock, queue, md5s
	lock = l
	queue = q
	md5s = d

def checkEmail(str):
	match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', str.lower())
	if match == None:
		print('Value Error: no valid email submitted')
		exit(1)

def download(arg):
	fileurl, dldir = arg
	filename = fileurl.split('/')[-1]
	dst = os.path.join(dldir, filename)

	#file download
	t = 0

	while True:
		t += 1
		try:
			response = urllib.request.urlretrieve(fileurl, dst)
		except (urllib.error.URLError, urllib.error.HTTPError, socket.timeout) as error:
			if t == 3:
				queue.put((fileurl, 0, error))
				break
			else:
				time.sleep(10)
				continue
		except Exception as e:
			queue.put((fileurl, 0, error))
			print('Caught exception while downloading', fileurl, ':', e)
			traceback.print_exc()
			print()
			raise e
			break
		else:

			#md5 download
			if not filename in md5s:
				md5file = os.path.join(os.path.dirname(fileurl), "md5checksums.txt")
				try:
					response = urllib.request.urlopen(md5file)
				except Exception as e:
					print('Caught exception while downloading', fileurl, ':', e)
					traceback.print_exc()
					print()
				else:
					for line in response:
						fields = line.decode(encoding='UTF-8').strip().split("  ")
						md5 = fields[0]
						file = fields[1][2:]
						md5s[file] = md5

			#md5 check
			if not filename in md5s:
				queue.put((fileurl, 1, "Note: md5 checksum not available"))
			elif hashlib.md5(open(dst, 'rb').read()).hexdigest() != md5s[filename]:
				queue.put((fileurl, 0, "md5 check failed"))
				os.remove(dst)
			else:
				queue.put((fileurl, 1, 'md5 checksum checked'))

			lock.acquire()
			completed.value += 1
			lock.release()
			queue.task_done()
			break

def gettextfile(ftp, filename, outfilehandle=None):
    # fetch a text file
	if outfilehandle is None:
		outfilehandle = sys.stdout
	# use a lambda to add newlines to the lines read from the server
	try:
		ftp.retrlines("RETR " + filename, lambda s, w=outfilehandle.write: w(s+"\n"))
	except Exception as e:
		print('Caught exception while downloading', filename)
		raise e

def getbinaryfile(ftp, filename, outfile=None):
    # fetch a binary file
    if outfile is None:
        outfile = sys.stdout
    ftp.retrbinary("RETR " + filename, outfile.write)

def readFasta(filename):
	entries = [] # [[header, lin_seq]]
	with gzip.open(filename, 'rb') as handle:
		for line in handle:
			line = line.decode(encoding='UTF-8')
			if line.lstrip()[0] == ">":
				entries.append([line, []])
			elif len(line) > 0 and len(entries) > 0:
				entries[-1][1].append(line)
	out = []
	for entry in entries:
		out.append([entry[0], ''.join(entry[1])])
	return out

def formatColumns(rows, empty='', space=5):
	'''formats nested list (1. level = rows; 2. level = columns) in equal-sized columns. fields containing integers or floats are aligned to the right'''

	#allowed signs in numbers
	allowed = ['+', '-', '.', ',', '%']

	#str conversion
	rows = [[str(x) for x in row] for row in rows]

	#fill up rows to same number of columns (important for transposing)
	maxlen = max([len(x) for x in rows])
	[x.extend([empty]*(maxlen-len(x))) for x in rows]
	align = []
	output = [''] * len(rows)
	for column in zip(*rows):
		#detect numbers to align right
		string = ''.join(column)
		for i in allowed:
			string = string.replace(i, '')
		if string.strip('+-.,').isdigit():
			align = 'r'
		else:
			align = 'l'

		maxlen = max([len(x) for x in column])
		for i in range(len(column)):
			if align == 'r':
				output[i] += ' ' * (maxlen - len(column[i])) + column[i] + ' ' * space
			else:
				output[i] += column[i] + ' ' * (maxlen - len(column[i])) + ' ' * space

	return '\n'.join(output) + '\n'

def cleanSeq(seq, clean = []):
	clean.extend('\r\n \t') #define chars to exclude here
	pattern = re.compile('|'.join(clean))
	return pattern.sub('',seq)

def countAmbig(seq, allow = []):
	allow.extend('ATGC')
	pattern = re.compile('|'.join(allow), re.IGNORECASE)
	return len(pattern.sub('',seq))

def ftp_connect(ftp_url, user='anonymous', pwd=""):
	try:
		ftp = FTP(ftp_url)
		ftp.login(user, pwd)
	except Exception as e:
		print(e)
		exit()
	return ftp

def ftp_close(ftp_obj):
	try:
		ftp_obj.quit()
	except Exception as e:
		ftp_obj.close()
	return True


def ftp_cd(ftp_obj, dir):
	try:
		ftp.cwd(dir)
	except all_errors as e:
		return False
	return True

def ftp_check_connect(ftp_obj):
	try:
		ftp_obj.voidcmd("NOOP")
	except:
		return False
	return True

def ask_user(question, yes= "y", no="n", autoyes=False):
	if autoyes:
		return True
	while True:
		prompt = input(question + " [" + str(yes) + "/" + str(no) + "] ")
		prompt = prompt.lower()
		if prompt == yes:
			return True
		elif prompt == no:
			return False


#START
socket.setdefaulttimeout(60)

if __name__ == '__main__':
	counter = 0
	checkEmail(args.email);
	if args.t > 3: #regarding personal communication to NCBI User Service and according to NCBI Website and Data Usage Policies and Disclaimers, it is not allowed to increase parallel downloads to more than 3!!!!
		args.t = 3
		print ("NOTE: number of parallel downloads have been restricted to 3.")
	elif args.t < 1:
		args.t = 1
		print ("NOTE: number of parallel downloads have been restricted to 1.")

	print(disclaimer)

	f = ftp_url[args.db].split('/')
	print("connecting to " + f[0] + " ...")
	ftp = ftp_connect(f[0], user='anonymous', pwd=args.email)

	if args.ftp:
		args.ftp = sorted(set(args.ftp))
		ftp_urls = args.ftp
	elif args.species:
		args.species = sorted(set(args.species))
		args.species = [x.replace(" ", "_") for x in args.species]
		ftp_urls = [ftp_url[args.db] + x + '/' for x in args.species]
	elif args.regex:
		cd = ftp_cd(ftp, "/" + "/".join(f[1:]))
		print("matching species ...")
		regex = re.compile(args.regex.replace(" ", "_"))
		species = [x for x in ftp.nlst() if regex.search(x)]
		ftp_urls = [ftp_url[args.db] + x + '/' for x in species]
		print(len(species), "species matched")
		print("matched species [n=" + str(len(species)) + "]: " + ", ".join([x.replace("_", " ") for x in species]))

	ftp_urls = [x.replace("\\", "/") for x in ftp_urls]
	ftp_urls = [x.replace("ftp://", "") for x in ftp_urls]
	ftp_urls = [x.split('/') for x in ftp_urls]

	if len(set([x[0] for x in ftp_urls])) > 1:
		exit("ERROR: ftp paths do not belong to the same domain.")

	ignored = []
	ignored_species = set()
	i = 0
	t = len(ftp_urls)
	for ftp_url in ftp_urls:
		i += 1
		print("checking assembly data ... [" + str(i) + "/" + str(t) + "]", end="\r")
		if not ftp_check_connect(ftp):
			ftp = ftp_connect(ftp_url[0], user='anonymous', pwd=args.email)

		cd = ftp_cd(ftp, "/" + "/".join(ftp_url[1:]))

		if not cd:
			print()
			if args.species:
				elem = ftp_url[-2].replace("_", " ")
				cd = ftp_cd(ftp, "/" + "/".join(ftp_url[1:-2]))
				if cd:
					ftpdirs = ftp.nlst()
					print("Could not find data for " + elem)
					print("Did you mean: " + ", ".join(difflib.get_close_matches(ftp_url[-2], ftpdirs, n=3)))
				else:
					print("Could not find data for " + elem)
			else:
				elem = ftp_url
				print("Could not find " + "/".join(ftp_url))

			user = ask_user("Ignore it?", autoyes=args.ignore)
			if user:
				ignored.append(ftp_url)
				ignored_species.add(elem)
			else:
				exit("downloads terminated on user request.")
		else:
			ftpdirs = ftp.nlst()
			if as_file not in ftpdirs:
				print()
				print("Could not find " + as_file + " in " + "/".join(ftp_url))
				user = ask_user("Ignore it?", autoyes=args.ignore)
				if user:
					ignored.append(ftp_url)
					ignored_species.add(ftp_url[-2].replace("_", " "))
					print("species ignored")
				else:
					exit("downloads terminated on user request.")
	print()

	#update ftp_urls
	ftp_urls = [x for x in ftp_urls if x not in ignored]

	#create dir
	err = 1
	while err > 0:
		timecode = time.strftime("%Y-%m-%d_%H-%M-%S")
		targetpath = "REFDOWNLOAD_" + timecode
		if os.path.isdir(targetpath):
			err += 1
			time.sleep(1)
		else:
			os.mkdir(targetpath)
			err = 0
		if err == 30:
			print("ERROR: giving up to find available output file name")
			sys.exit(1)

	print ("download folder created: " + targetpath + "/")

	#download assembly summary
	l = len(ftp_urls)
	as_files = []
	for i in range(l):
		print("downloading assembly summaries ... [" + str(i+1) + "/" + str(l) + "]", end="\r")
		as_files.append(os.path.join(targetpath, str(i) + "_" + as_file))
		cd = ftp_cd(ftp, "/" + "/".join(ftp_urls[i][1:]))
		with open(as_files[-1], 'w') as handle:
			gettextfile(ftp, as_file, handle)
	print()

	#processing assembly summary
	datasets = []
	print("processing assembly summaries ...")
	for as_file in as_files:
		handle = open(as_file, 'r')
		datasets.append({}) #stores status as key and urls as list
		header = None
		for line in handle:
			line = line.strip()
			if line.find("# assembly_accession") == 0:
				fields = line.split("\t")
				status_key = fields.index('assembly_level')
				url_key = fields.index('ftp_path')
				version_key = fields.index('version_status')
				header = True
			elif header and len(line.strip()) > 0 and line[0] != "#":
				fields = line.split("\t")
				status = fields[status_key]
				url = fields[url_key]
				vers = fields[version_key]
				if vers == "latest":
					if status not in datasets[-1]:
						datasets[-1][status] = []
					datasets[-1][status].append(url)



	#status selection
	stati = set()
	for dataset in datasets:
		stati.update(list(dataset.keys()))
	stati = sorted(stati)

	data = []
	delstati = []
	logging['status_selection'] = {}
	for status in stati:
		i = 1
		l = sum([len(x[status]) for x in datasets if status in x])
		user = ask_user("Downloading " + str(l) + " datasets marked as " + status + "?")
		if user:
			logging['status_selection'][status] = "considered"
		else:
			delstati.append(status)
			logging['status_selection'][status] = "not considered"

	for status in delstati:
		stati.remove(status)
		for i in range(len(datasets)):
			if status in datasets[i]:
				del(datasets[i][status])

	filter(None, datasets)

	if len(datasets) == 0:
		exit("nothing to download (no datasets selected).")

	#screening file types
	print("screening ftp directories ...", end = " ")
	filetypes = []
	files = {}
	filestatus = {}
	total = 0
	for status in stati:
		total += sum([len(x[status]) for x in datasets if status in x])

	urls = set()
	for status in stati:
		for dataset in datasets:
			if status in dataset:
				urls.update(dataset[status])

	i = 0
	for url in urls:
		if not ftp_check_connect(ftp):
			ftp = ftp_connect(ftp_url[0], user='anonymous', pwd=args.email)
		i += 1
		ac = url.split("/")[-1]
		url = url.replace('ftp://ftp.ncbi.nlm.nih.gov', '').replace('https://ftp.ncbi.nlm.nih.gov', '')
		try:
			ftp.cwd(url)
			filelist = ftp.nlst()
		except:
			ftp = ftp_connect(ftp_url[0], user='anonymous', pwd=args.email)
			ftp.cwd(url)
			filelist = ftp.nlst()
		for file in filelist:
			if not file in exclude_files:
				filetype = file.replace(ac, '')
				if not filetype in filetypes:
					filetypes.append(filetype)
					files[filetype] =[]
				#fileurl = "ftp://anonymous:" + args.email + "@" + "ftp.ncbi.nlm.nih.gov" + url + "/" + file
				fileurl = "ftp://" + "ftp.ncbi.nlm.nih.gov" + url + "/" + file
				files[filetype].append(fileurl)
				filestatus[fileurl] = status
		print("\rscreening ftp directories ...   [" + str(i) + "/" + str(total) + "]       ", end = " ")
	print("\rscreening ftp directories ...   [" + str(i) + "/" + str(total) + "]       ")

	for filetype in filetypes:
		user = ask_user("Downloading " + str(len(files[filetype])) + "x '*" + filetype + "' files?")
		if not user:
			del(files[filetype])

	if len(files) == 0:
		exit("nothing to download (no file type selected).")

	ftp_close(ftp)

	#preparing download
	print("preparing downloads ...")
	fileurls = []
	dirs = {}
	dldirs = []
	letters = []
	letters.extend('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')

	for status in stati:
		dldir = status
		for ch in dldir:
			if not ch in letters:
				dldir = dldir.replace(ch, "_")
		dirs[status]= os.path.join(targetpath, dldir)
		os.mkdir(dirs[status])

	for filetype in files.keys():
		for fileurl in files[filetype]:
			status = filestatus[fileurl]
			fileurls.append(fileurl)
			dldirs.append(dirs[status])

	completed = multiprocessing.Value('i', 0)
	l = multiprocessing.Lock()
	m = multiprocessing.Manager()
	d = m.dict()
	q = m.Queue()
	p = multiprocessing.Pool(processes=args.t, initializer=init, initargs=(l,q,d,))
	rs = p.map_async(download, zip(fileurls, dldirs))
	c = 0

	while (True):
		if (rs.ready()):
			break
		if q.qsize() != c:
			c = q.qsize()
			print ("\r" + str(c) + "/" + str(len(fileurls)) + " downloads processed (press Ctrl+C to abort)   ", end = " ")
		time.sleep(1)


	q.join()
	p.close()
	p.join()

	success = []
	failed = []

	while q.qsize():
		item = q.get()
		if item[1] == 0:
			failed.append([item[0], item[2]])
		else:
			success.append([item[0], item[2]])


	if len(failed) == 0:
		print("\rall downloads successfully completed                                     ")
	else:
		print("\r" + str(len(success)) + " downloads successfully completed                                ")
		print(str(len(failed)) + " downloads failed (see " + os.path.join(targetpath,  "refDownloader.log for details") + ")")

	logging['download_success'] = success
	logging['download_failed'] = failed


	#sorting genomic and plasmidic
	if '_genomic.fna.gz' in files and args.sort == True:
		print ("sorting fna entries ...", end = " ")
		logging['sorting'] = {}
		keywords = [['genom', 'genomes'], ['chromosome', 'genomes'], ['plasmid', 'plasmids']] #[[keyword, folder]]
		unknown_dir = 'unknown'
		dldirs = set(dldirs)

		genomic_files = []
		for file in files['_genomic.fna.gz']:
			genomic_files.append(os.path.basename(file))

		created_subdirs = []
		for dldir in dldirs:
			files2sort = []
			for filename in os.listdir(dldir):
				if filename in genomic_files:
					files2sort.append(filename)

			l = len(files2sort)
			k = 0
			for filename in files2sort:
				k += 1
				print ("\rsorting fna entries ...  [" + str(k) + "/" + str(l) + "]         ", end = " ")


				i = 0
				fasta_entries = readFasta(os.path.join(dldir, filename))

				for entry in fasta_entries:
					newfilename = filename[:-len("_genomic.fna.gz")] + "_" + str(i) + "_genomic.fna"
					logging['sorting'][newfilename] = []
					i += 1
					header = entry[0]
					seq = cleanSeq(entry[1])
					grouped = 0

					j = []
					for keyword, folder in keywords:
						if header.find(keyword) > 0:
							j.append(folder)
					if len(set(j)) == 1:
						subdir = j[0]
					else:
						subdir = unknown_dir

					dir = os.path.join(dldir, subdir)
					if not dir in created_subdirs:
						os.mkdir(dir)
						created_subdirs.append(dir)

					with open(os.path.join(dir, newfilename), 'w') as handle:
						handle.write(header+seq)

					logging['sorting'][newfilename].append((subdir, header, len(seq), countAmbig(seq)))
print ()

#log filing
print ("writing log file ... ")
with open(os.path.join(targetpath, logfilename), "w") as loghandle:
	#general
	loghandle.write("REFDOWNLOADER\n")
	loghandle.write(logsep)
	log=[["version:", version], ['time:', timecode], ['result folder:', targetpath]]

	if args.species:
		species = []
		for s in args.species:
			s = s.replace("_", " ")
			if s in ignored_species:
				s += " (not existent, ignored)"
			species.append(s)
		log.append(["input [species]:", species[0]])
		for s in species[1:]:
			log.append(["", s])
	elif args.ftp:
		ftps = []
		for f in args.ftp:
			if f in ignored:
				f += " (not existent, ignored)"
			ftps.append(f)
		log.append(["input [ftp paths]:", ftps[0]])
		for f in ftps[1:]:
			log.append(["", f])
	elif args.regex:
		log.append(["input [regex]:", args.regex])
		col1 = "matched species [n=" + str(len(species)) + "]:"
		for s in species:
			s = s.replace("_", " ")
			if s in ignored_species:
				s += " (no assembly report available, ignored)"
			log.append([col1, s.replace("_", " ")])
			col1 = ""

	if args.sort == True:
		log.append(["sorting:", "yes"])
		log.append(["", "please consider, that sorting can be only applied to *_genomic.fna.gz files"])
	else:
		log.append(["sorting:", "no"])
	loghandle.write(formatColumns(log))
	print(formatColumns(log))

	#file types
	loghandle.write("\n\nDOWNLOADED FILE TYPES\n")
	loghandle.write(logsep)
	for filetype in list(files.keys()):
		loghandle.write(filetype + '\n')

	#status
	loghandle.write("\n\nASSEMBLY LEVEL\n")
	loghandle.write(logsep)
	log = []
	for status in logging['status_selection']:
		log.append([status + ":", logging['status_selection'][status]])
	loghandle.write(formatColumns(log))

	#download
	loghandle.write("\n\nDOWNLOAD SUMMARY\n")
	loghandle.write(logsep)
	log = []
	log.append(["total downloads:", len(logging['download_success']) + len(logging['download_failed'])])
	log.append(["successful downloads:", len(logging['download_success'])])
	log.append(["failed downloads:", len(logging['download_failed'])])
	log.append([''])
	loghandle.write(formatColumns(log))

	if len(logging['download_success']) > 0:
		loghandle.write("\n\nSUCCESSFUL DOWNLOADS\n")
		loghandle.write(logsep)
		log = []
		for dl in logging['download_success']:
			#dl[0] = 'ftp://' + dl[0].split('@')[-1] #remove email and ananymous from ftp adress
			log.append(dl)
		loghandle.write(formatColumns(log))


	if len(logging['download_failed']) > 0:
		loghandle.write("\n\nFAILED DOWNLOADS\n")
		loghandle.write(logsep)
		log = []
		for dl in logging['download_failed']:
			dl[0] = 'ftp://' + dl[0].split('@')[-1] #remove email and ananymous from ftp adress
			log.append(dl)
		loghandle.write(formatColumns(log))


	#DOWNLOADED REPLICONS
	if '_genomic.fna.gz' in files and args.sort == True:
		loghandle.write("\n\nDOWNLOADED REPLICONS\n")
		loghandle.write(logsep)
		log = [['type', 'header', 'length [bp]', 'ambigous sites', 'file name']]
		for file in logging['sorting']:
			for entry in logging['sorting'][file]:
				type = entry[0]
				header = entry[1]
				seqlen = entry[2]
				ambilen = entry[3]
				log.append([type, header.strip(), seqlen, ambilen, file])
		loghandle.write(formatColumns(log))
